let gestionTablero = new GestionTablero();
let tablero = gestionTablero.getTablero();
let turno = 1;
//let tabla.rows[i].cells[i] y no hace falta crear con el queryselector(tr)

function marcarCelda(e) {
    let eleccion = e.target;

    const tr = new Array(6);
    let rows = document.querySelectorAll('tr');
    for (let cont = 0; cont< tr.length; cont++) {
        tr[cont] = Array.from(document.getElementsByTagName('tr')[cont].querySelectorAll('td'));
    }

    for (let i = 0; i < tr.length ; i++) {
        if (rows[i] == eleccion.parentElement.parentElement.parentElement) {
            for (let j =0 ; j < tr[i].length; j++) {
                if (tr[i][j] == eleccion.parentElement.parentElement) {  
                    if (gestionTablero.cambiarCasilla(i, j, turno)) { // modelo 
                        if (turno == 1) {
                            turno++;
                            document.getElementById('cuadroMensaje').textContent = 'Turno de J2';
                        } else if (turno == 2) {
                            turno--;
                            document.getElementById('cuadroMensaje').textContent = 'Turno de J1';
                        }
                    } 
                    for (let contFilas = 0; contFilas<tablero.length; contFilas++) { //vista
                        for (let contColumnas = 0; contColumnas < tablero[contFilas].length; contColumnas++) {
                            if (tablero[contFilas][contColumnas] == 1) {
                                tr[contFilas][contColumnas].firstChild.firstChild.className = 'casillaJ1';
                            } else if (tablero[contFilas][contColumnas] == 2) {
                                tr[contFilas][contColumnas].firstChild.firstChild.className = 'casillaJ2';
                            }
                        }
                    } //fin vista
                }
            }
        }
    }

    if (gestionTablero.comprobarVictoria() == 1) document.getElementById('cuadroMensaje').textContent = 'El ganador es: J1';
    else if (gestionTablero.comprobarVictoria() == 2) document.getElementById('cuadroMensaje').textContent = 'El ganador es J2';
}

function main() {
    document.getElementById('tabla').addEventListener('click', (e) => marcarCelda(e));
}

window.addEventListener('load',main);