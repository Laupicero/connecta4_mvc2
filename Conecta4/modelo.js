class GestionTablero {

    constructor(tablero) {
        this.tablero = new Array(6);
        this.tablero[0] = new Array(7);
        this.tablero[1] = new Array(7);
        this.tablero[2] = new Array(7);
        this.tablero[3] = new Array(7);
        this.tablero[4] = new Array(7);
        this.tablero[5] = new Array(7);

        for (let i = 0; i< this.tablero.length; i++) {
            for (let j = 0; j<this.tablero[i].length; j++) {
                this.tablero[i][j] = 0;
            }
        }
    }

    getTablero() {
        return this.tablero;
    }

    cambiarCasilla(i, j, jugador) {
        let cambioExitoso = false;

        if (tablero[i][j] == 0) {
            for (let cont = tablero.length-1;cont >= 0; cont--) {
                if (tablero[cont][j] == 0) {
                    tablero[cont][j] = jugador;
                    break;
                } 
            }
            cambioExitoso = true;
            console.log(tablero);
        }

        return cambioExitoso;
    }

    comprobarVictoria() {
        let contJ1 = 0;
        let contJ2 = 0;
        let ganador = 0;

        for (let i=0; i < tablero.length; i++ ) { //horizontales
            for (let ind=0; ind < tablero[i].length; ind++) {
                if (tablero[i][ind] == 1) {
                    contJ1++;
                    contJ2 = 0;
                    if (contJ1 == 4) {
                        return ganador = 1;
                    }
                } else if (tablero[i][ind] == 2) {
                    contJ2++;
                    contJ1 = 0;
                    if (contJ2 == 4) {
                        return ganador = 2;
                    }
                } else {
                    contJ1 = 0;
                    contJ2 = 0;
                }
            }
            contJ1 = 0;
            contJ2 = 0;
        }

        contJ1 = 0;
        contJ2 = 0;

        for (let i=0; i < tablero.length+1; i++ ) { //verticales
            for (let ind=0; ind < tablero.length; ind++) {
                if (tablero[ind][i] == 1) {
                    contJ1++;
                    contJ2 = 0;
                    if (contJ1 == 4) {
                        return ganador = 1;
                    }
                } else if (tablero[ind][i] == 2) {
                    contJ2++;
                    contJ1 = 0;
                    if (contJ2 == 4) {
                        return ganador = 2;
                    }
                } else {
                    contJ1 = 0;
                    contJ2 = 0;
                }
            }
            contJ1 = 0;
            contJ2 = 0;
        }

        contJ1=0;
        contJ2=0;

        let cont = 0;
        while (cont < 4) { //diagonales descendentes mitad de arriba
            for (let i = 0; i< tablero.length; i++) { 
                if (tablero[i][i+cont] == 1) {
                    contJ1++;
                    contJ2 = 0;
                    if (contJ1 == 4) {
                        return ganador = 1;
                    }
                } else if (tablero[i][i+cont] == 2) {
                    contJ2++;
                    contJ1 = 0;
                    if (contJ2 == 4) {
                        return ganador = 2;
                    }
                } else {
                    contJ1 = 0;
                    contJ2 = 0;
                }
            }
            cont++;
        }

        contJ1 = 0;
        contJ2 = 0;
        
        cont = 1
        while (cont < 3) { //diagonales descendentes mitad de abajo sin mitad
            for (let i = 0; i< tablero.length-cont; i++) { 
                if (tablero[i+cont][i] == 1) {
                    contJ1++;
                    contJ2 = 0;
                    if (contJ1 == 4) {
                        return ganador = 1;
                    }
                } else if (tablero[i+cont][i] == 2) {
                    contJ2++;
                    contJ1 = 0;
                    if (contJ2 == 4) {
                        return ganador = 2;
                    }
                } else {
                    contJ1 = 0;
                    contJ2 = 0;
                }
            }
            cont++;
        }

        contJ1 = 0;
        contJ2 = 0;

        cont=0;
        while (cont < 3) { //diagonales ascendentes mitad de abajo
            for (let i = 0, j= tablero.length; i< tablero.length-cont && j>=0; i++, j--) { 
                if (tablero[i+cont][j] == 1) {
                    contJ1++;
                    contJ2 = 0;
                    if (contJ1 == 4) {
                        return ganador = 1;
                    }
                } else if (tablero[i+cont][j] == 2) {
                    contJ2++;
                    contJ1 = 0;
                    if (contJ2 == 4) {
                        return ganador = 2;
                    }
                } else {
                    contJ1 = 0;
                    contJ2 = 0;
                }
            }
            cont++;
        }

        contJ1 = 0;
        contJ2 = 0;
        
        cont = 1;
        while (cont < 4) { //diagonales ascendentes mitad de arriba sin mitad
            for (let i = 0, j= tablero.length-cont; i< tablero.length && j>=0; i++, j--) { 
                if (tablero[i][j-cont] == 1) {
                    contJ1++;
                    contJ2 = 0;
                    if (contJ1 == 4) {
                        return ganador = 1;
                    }
                } else if (tablero[i][j-cont] == 2) {
                    contJ2++;
                    contJ1 = 0;
                    if (contJ2 == 4) {
                        return ganador = 2;
                    }
                } else {
                    contJ1 = 0;
                    contJ2 = 0;
                }
            }
            cont++;
        }

        return ganador;
    }
}